﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.UnitTests.Builders;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers
{
    public class PartnersControllerTests
    {
        private readonly Mock<IRepository<Partner>> _partnersRepositoryMock;
        private readonly PartnersController _partnersController;
        
        public PartnersControllerTests()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _partnersRepositoryMock = fixture.Freeze<Mock<IRepository<Partner>>>();
            _partnersController = fixture.Build<PartnersController>().OmitAutoProperties().Create();
        }

        
        private Partner GetBasePartner()
        {
            var partner = PartnerBuilder.CreateBasePartner();

            return partner;
        }

        /// <summary>
        /// Тест на состояние
        /// </summary>
        [Theory]
        [InlineData("def47943-7aaf-44a1-ae21-05aa4948b165")]
        [InlineData("7d994823-8226-4273-b063-1a95f3cc1df8")]
        public async void CanNotSetNotActivePartnerPromoCodeLimitAsync(string partnerIdStr)
        {
            // Arrange
            var partnerId = Guid.Parse(partnerIdStr);
            var partner = new Partner()
            {
                Id = Guid.Parse("7d994823-8226-4273-b063-1a95f3cc1df8"),
                Name = "Суперигрушки",
                IsActive = true,
                PartnerLimits = new List<PartnerPromoCodeLimit>()
                {
                    new PartnerPromoCodeLimit()
                    {
                        Id = Guid.Parse("e00633a5-978a-420e-a7d6-3e1dab116393"),
                        CreateDate = new DateTime(2020, 07, 9),
                        EndDate = new DateTime(2020, 10, 9),
                        Limit = 100
                    }
                }
            }; 

            var request = new SetPartnerPromoCodeLimitRequest();

            var mock = new Mock<IRepository<Partner>>();
            mock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);
            var mockCurrentDateTimeProvider = new Mock<ICurrentDateTimeProvider>();
            mockCurrentDateTimeProvider.Setup(x => x.CurrentDateTime)
                .Returns(new DateTime(2020, 10, 14));
            var controller = new PartnersController(mock.Object);
 
            // Act
            var result = await controller.SetPartnerPromoCodeLimitAsync(partnerId, request);
 
            // Assert
            Assert.IsType<BadRequestObjectResult>(result);
        }
        
        /// <summary>
        /// Тест на поведение
        /// </summary>
        [Fact]
        public async void CanSetActivePartnerPromoCodeLimitAsyncAndGotPartnerFromRepository()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            var request = new SetPartnerPromoCodeLimitRequest();

            var partnerRepositoryMock = new Mock<IRepository<Partner>>();
            var mockCurrentDateTimeProvider = new Mock<ICurrentDateTimeProvider>();
            mockCurrentDateTimeProvider.Setup(x => x.CurrentDateTime)
                .Returns(new DateTime(2020, 10, 14));
            var controller = new PartnersController(partnerRepositoryMock.Object);
 
            // Act
            var result = await controller.SetPartnerPromoCodeLimitAsync(partnerId, request);
 
            // Assert
            partnerRepositoryMock.Verify(x=> x.GetByIdAsync(partnerId), Times.Once);
        }
        
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotActive_ShouldReturnBadRequest()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            var partner = new Partner()
            {
                Id = Guid.Parse("7d994823-8226-4273-b063-1a95f3cc1df8"),
                Name = "Суперигрушки",
                IsActive = true,
                PartnerLimits = new List<PartnerPromoCodeLimit>()
                {
                    new PartnerPromoCodeLimit()
                    {
                        Id = Guid.Parse("e00633a5-978a-420e-a7d6-3e1dab116393"),
                        CreateDate = new DateTime(2020, 07, 9),
                        EndDate = new DateTime(2020, 10, 9),
                        Limit = 100
                    }
                }
            };
            
            var request = new SetPartnerPromoCodeLimitRequest();
            
            var mock = new Mock<IRepository<Partner>>();
            mock.Setup(x => x.GetByIdAsync(partnerId)).ReturnsAsync(partner);
            var mockCurrentDateTimeProvider = new Mock<ICurrentDateTimeProvider>();
            mockCurrentDateTimeProvider.Setup(x => x.CurrentDateTime)
                .Returns(new DateTime(2020, 10, 14));
            var controller = new PartnersController(mock.Object);
 
            // Act
            var result = await controller.SetPartnerPromoCodeLimitAsync(partnerId, request);
 
            // Assert
            Assert.IsType<BadRequestObjectResult>(result);
        }

        [Fact]
        public async Task CancelPartnerPromoCodeLimitAsync_HasActiveLimit_ShouldSetCancelDateNow()
        {
            //Arrange
            var partner = GetBasePartner().WithOnlyOneActiveLimit();
            var targetLimit = partner.PartnerLimits.First();
            var partnerId = partner.Id;
            var now = new DateTime(2020, 10, 14);
            var expected =
                new PartnerPromoCodeLimit()
                {
                    Id = Guid.Parse("e00633a5-978a-420e-a7d6-3e1dab116393"),
                    CreateDate = new DateTime(2020, 07, 9),
                    CancelDate = now,
                    EndDate = new DateTime(2020, 10, 9),
                    Limit = 100
                };

            var mock = new Mock<IRepository<Partner>>();
            mock.Setup(x => x.GetByIdAsync(partnerId)).ReturnsAsync(partner);
            var mockCurrentDateTimeProvider = new Mock<ICurrentDateTimeProvider>();
            mockCurrentDateTimeProvider.Setup(x => x.CurrentDateTime)
                .Returns(now);

            var controller = new PartnersController(mock.Object);

            await controller.CancelPartnerPromoCodeLimitAsync(partnerId);

            targetLimit.Should().BeEquivalentTo(expected);
        }
    }
}